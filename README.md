# The Advent of Code 2022

My attempts at the Advent of Code 2022, using Rust. It has tests where I thought of writing some, no guarantees.

You will need [Rust > 1.65.0](https://www.rust-lang.org/) to run them.

## Code organisation

Each day of the calendar is in its own module in `src/`.

Per how `cargo aoc` works:

- The `generator` functions (prefixed with `generate_`) help parse the inputs
  into a useable format whose specificity depends on the puzzle of the day
  (maybe simple `u32`, maybe whole-ass enums, wow).
- The `solver` functions (prefixed with `solve_`) take care of the main
  algorithm for the solution. I tried to be as clear and explicit in how they
  work, this is as much a fun project as an exercise in readability and
  maintainability for me.

## Running the solutions

```sh
git clone git@gitlab.com:elarcis/advent-of-code-2022.git
cd ./advent-of-code-2022
cargo install cargo-aoc

cargo aoc
```

Alternatively, if you do not wish to install `cargo-aoc` you can run the whole
of the calendar using `cargo run -r`.

[cargo-aoc]: https://github.com/gobanos/cargo-aoc
