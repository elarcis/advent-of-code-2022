use aoc_runner_derive::aoc;
use itertools::Itertools;

// Puzzle: https://adventofcode.com/2022/day/3

/// Solves part 1 by splitting each line in two parts of equal lengths.
///
/// The algorithm works by finding the first byte in the second part that is also contained in the
/// first one.
///
/// This works with the following assumptions given in the puzzle:
/// * Only one mistake per rucksack
/// * The mistake is when one item is present in both parts of the sack
///
#[must_use]
#[aoc(day3, part1)]
pub fn solve_part1(input: &str) -> u32 {
    input
        .lines()
        .map(|line| line.split_at(line.len() / 2))
        .filter_map(|(pack1, pack2)| {
            let pack1 = pack1.as_bytes();
            let mistake = pack2.as_bytes().iter().find(|c| pack1.contains(c))?;
            Some(u32::from(get_item_priority(*mistake)))
        })
        .sum()
}

/// Solves part 2 using nested `.fold()` calls.
///
/// First step is to build, for each elf in a group, an inventory of the items they possess.
/// Since there are only 52 possible items, they are stored in a `[bool; 52]` accumulator, where
/// each element is matched to a given priority.
///
/// Once every elf in the group has been processed, the three accumulator’s contents are compared so
/// that the badge is identified as the single element present both three accumulators.
///
#[must_use]
#[aoc(day3, part2, fold)]
pub fn solve_part2_fold(input: &str) -> u32 {
    input
        .lines()
        .chunks(3)
        .into_iter()
        .filter_map(|group| {
            let mut counts = group.map(get_elf_items);
            let elves = [counts.next()?, counts.next()?, counts.next()?];
            let badge_index = (0..52).find(|i| elves[0][*i] && elves[1][*i] && elves[2][*i])?;
            Some(u32::try_from(badge_index).expect("badge index should always be < 52") + 1)
        })
        .sum()
}

/// Solves part 2 using a simple character lookup.
///
/// Each elf’s full rucksack is examined as a slice of bytes, and the badge is identified as the
/// first byte in `elves[0]` that is also contained in `elves[1]` and `elves[2]`’s inventories.
///
/// The badge’s priority is then calculated once, by opposition to the `fold` method that computes
/// priorities for every item. This makes `solve_part2_contains` significantly faster than its
/// counterpart.
///
#[must_use]
#[aoc(day3, part2, lookup)]
pub fn solve_part2_contains(input: &str) -> u32 {
    input
        .lines()
        .chunks(3)
        .into_iter()
        .filter_map(|mut group| {
            let elves = [
                group.next()?.as_bytes(),
                group.next()?.as_bytes(),
                group.next()?.as_bytes(),
            ];

            elves[0]
                .iter()
                .find(|item| elves[1].contains(item) && elves[2].contains(item))
                .map(|badge| u32::from(get_item_priority(*badge)))
        })
        .sum()
}

/// Solves part 2 by building one bitflag per elf to count which item they own.
///
/// The badge is simply the bit that’s in both three bitflags, which can be computed through a
/// bitwise `AND` operation.
///
/// This implementation isn’t in theory much faster than comparing arrays, but an interesting
/// micro-optimisation makes it actually twice as fast, see [`get_elf_bitflags`].
///
#[must_use]
#[aoc(day3, part2, bitwise)]
pub fn solve_part2_bitwise(input: &str) -> u32 {
    input
        .lines()
        .chunks(3)
        .into_iter()
        .filter_map(get_badge)
        .sum()
}

/// Gets the priority of the badge for a given group of elves.
///
/// If `group` contains less than 3 elves, `None` is returned.
///
fn get_badge<'i>(mut group: impl Iterator<Item = &'i str>) -> Option<u32> {
    let elves = [
        get_elf_bitflags(group.next()?.as_bytes()),
        get_elf_bitflags(group.next()?.as_bytes()),
        get_elf_bitflags(group.next()?.as_bytes()),
    ];

    let badge = elves[0].0 & elves[1].0 & elves[2].0;
    Some(badge.trailing_zeros())
}

struct ElfItems(u64);

/// Gets the items one `elf` possesses in a `ElfItems` bitflag.
///
/// Each item’s priority is mapped to a different power of two, which means `ElfItems` can be
/// subjected to bitwise operations in order to solve the puzzle.
///
/// Please note that priority `1` is mapped to `2^1` and so on, meaning `2^0` is unused.
///
fn get_elf_bitflags(elf: &[u8]) -> ElfItems {
    elf.iter().fold(ElfItems(0), |mut acc, byte| {
        let priority = if *byte >= b'a' {
            byte - b'a'
        } else {
            byte - b'A' + 26
        } + 1; // Somehow that `+ 1` makes the whole solver TWICE as fast, and I don’t. Know. Why.
        acc.0 |= 1 << priority;
        acc
    })
}

/// Gets a table of whether each priority (item) was found within an elf’s rucksack.
///
fn get_elf_items(elf: &str) -> [bool; 52] {
    elf.as_bytes()
        .iter()
        .fold([false; 52], |mut elf_acc, item| {
            elf_acc[usize::from(get_item_priority(*item) - 1)] = true;
            elf_acc
        })
}

/// Gets the priority of an inventory item.
///
/// If the item is not known, its priority will be `0`.
///
fn get_item_priority(item: u8) -> u8 {
    match item {
        val @ b'a'..=b'z' => val - b'a' + 1,
        val @ b'A'..=b'Z' => val - b'A' + 27,
        _ => 0,
    }
}

#[cfg(test)]
mod tests {
    use color_eyre as ce;
    use indoc::indoc;

    use super::*;

    const INPUT: &str = "./input/2022/day3.txt";
    const TEST_INPUT: &str = indoc! { r#"
        vJrwpWtwJgWrhcsFMMfFFhFp
        jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
        PmmdzqPrVvPwwTWBwg
        wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
        ttgJtRGJQctTZtZT
        CrZsJsPPZsGzwwsLwLmpwMDw
    "#};

    /// Correct answer for the part 1 using the test input
    const PART_1_TEST_ANSWER: u32 = 157;
    /// Correct answer for the part 2 using the test input
    const PART_2_TEST_ANSWER: u32 = 70;

    /// Correct answer for the part 1
    const PART_1_ANSWER: u32 = 8185;
    /// Correct answer for the part 2
    const PART_2_ANSWER: u32 = 2817;

    #[test]
    fn it_solves_part1_test() {
        assert_eq!(solve_part1(TEST_INPUT), PART_1_TEST_ANSWER);
    }

    #[test]
    fn it_solves_part1() -> ce::Result<()> {
        let input = std::fs::read_to_string(INPUT)?;
        assert_eq!(solve_part1(&input), PART_1_ANSWER);
        Ok(())
    }

    #[test]
    fn it_solves_part2_test() {
        assert_eq!(solve_part2_bitwise(TEST_INPUT), PART_2_TEST_ANSWER);
    }

    #[test]
    fn it_solves_part2_fold() -> ce::Result<()> {
        let input = std::fs::read_to_string(INPUT)?;
        assert_eq!(solve_part2_fold(&input), PART_2_ANSWER);
        Ok(())
    }

    #[test]
    fn it_solves_part2_contains() -> ce::Result<()> {
        let input = std::fs::read_to_string(INPUT)?;
        assert_eq!(solve_part2_contains(&input), PART_2_ANSWER);
        Ok(())
    }

    #[test]
    fn it_solves_part2_bitwise() -> ce::Result<()> {
        let input = std::fs::read_to_string(INPUT)?;
        assert_eq!(solve_part2_bitwise(&input), PART_2_ANSWER);
        Ok(())
    }

    #[test]
    fn it_gets_item_priorities() {
        assert_eq!(get_item_priority(b'a'), 1);
        assert_eq!(get_item_priority(b'z'), 26);
        assert_eq!(get_item_priority(b'A'), 27);
        assert_eq!(get_item_priority(b'Z'), 52);
        assert_eq!(get_item_priority(b'@'), 0);
    }
}
