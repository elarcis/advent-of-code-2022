use aoc_runner_derive::{aoc, aoc_generator};
use ce::eyre::{eyre, Context};
use color_eyre as ce;

// Puzzle: https://adventofcode.com/2022/day/1

/// Parses every line of the input as either a `None` (blank line) or `Some(u32)` (calories).
///
/// # Errors
///
/// If at any point in the parsing of the input a line isn’t blank or a number, an adequate error
/// will be returned.
///
#[aoc_generator(day1)]
pub fn generate_calories(input: &str) -> ce::Result<Vec<Option<u32>>> {
    input
        .lines()
        .enumerate()
        .map(|(number, line)| {
            if line.is_empty() {
                Ok(None)
            } else {
                line.parse()
                    .wrap_err(eyre!(
                        "could not parse line #{}, `{}`, into a number",
                        number + 1,
                        line
                    ))
                    .map(Some) // The `Ok` must still contain a `Some`.
            }
        })
        .collect::<Result<_, _>>()
}

#[must_use]
#[aoc(day1, part1)]
pub fn solve_part1(input: &[Option<u32>]) -> u32 {
    get_calories_sums(input)
        .into_iter()
        .max()
        .expect("there should always be at least one elf with calories")
}

/// Solves the part 2 with a naive approach of sorting every entry and taking the biggest 3.
/// It revolves around one single big sort of ≈ 250 elements.
///
#[must_use]
#[aoc(day1, part2, naive)]
pub fn solve_part2_naive(input: &[Option<u32>]) -> u32 {
    let mut calories = get_calories_sums(input);
    calories.sort_unstable();
    calories.into_iter().rev().take(3).sum()
}

/// Solves the part 2 with a more involved approach of keeping a reccord of 3 max values, that I
/// will call the “podium”.
///
/// Each element is compared against max values until one is found smaller, that means the current
/// element needs to be put on the podium, and replace the smallest max value.
///
/// Since the podium only needs to be sorted when inserting a new max value, this consists of less
/// than ≈ 250 sorts of a 3-elements array, which would be marginally faster than a single sort of
/// ≈ 250 elements.
///
#[must_use]
#[aoc(day1, part2, involved)]
pub fn solve_part2_involved(input: &[Option<u32>]) -> u32 {
    let sums = get_calories_sums(input);
    get_n_maxes::<3>(sums).into_iter().sum()
}

/// Gets the `T` largest values in the `input`.
///
fn get_n_maxes<const T: usize>(input: impl IntoIterator<Item = u32>) -> [u32; T] {
    input.into_iter().fold([0; T], |mut acc, value| {
        if acc.iter().any(|&max| max < value) {
            acc.sort_unstable();
            acc[0] = value;
        }
        acc
    })
}

/// Sums each consecutive `Some` element of the input into a `Vec`.
///
/// Each `None` pushes a new element into the `Vec` and subsequent elements will be added to it
/// instead.
///
fn get_calories_sums(input: &[Option<u32>]) -> Vec<u32> {
    input.iter().fold(vec![0], |mut acc, calories| {
        match calories {
            None => acc.push(0),
            Some(calories) => {
                let last = acc
                    .last_mut()
                    .expect("accumulator `Vec` should always have one starting element");
                *last += calories;
            }
        };
        acc
    })
}

#[cfg(test)]
mod tests {
    use color_eyre as ce;
    use indoc::indoc;

    use super::*;

    const INPUT: &str = "./input/2022/day1.txt";
    const TEST_INPUT: &str = indoc! {r#"
        1000
        2000
        3000

        4000

        5000
        6000

        7000
        8000
        9000

        10000
    "#};

    /// Correct answer for the part 1 using the test input
    const PART_1_TEST_ANSWER: u32 = 24000;
    /// Correct answer for the part 2 using the test input
    const PART_2_TEST_ANSWER: u32 = 45000;

    /// Correct answer for the part 1
    const PART_1_ANSWER: u32 = 68802;
    /// Correct answer for the part 2
    const PART_2_ANSWER: u32 = 205370;

    #[test]
    fn it_solves_part1_test() -> ce::Result<()> {
        let input = generate_calories(TEST_INPUT)?;
        assert_eq!(solve_part1(&input), PART_1_TEST_ANSWER);
        Ok(())
    }

    #[test]
    fn it_solves_part1() -> ce::Result<()> {
        let input = generate_input()?;
        assert_eq!(solve_part1(&input), PART_1_ANSWER);
        Ok(())
    }

    #[test]
    fn it_solves_part2_naive_test() -> ce::Result<()> {
        let input = generate_calories(TEST_INPUT)?;
        assert_eq!(solve_part2_naive(&input), PART_2_TEST_ANSWER);
        Ok(())
    }

    #[test]
    fn it_solves_part2_involved_test() -> ce::Result<()> {
        let input = generate_calories(TEST_INPUT)?;
        assert_eq!(solve_part2_involved(&input), PART_2_TEST_ANSWER);
        Ok(())
    }

    #[test]
    fn it_solves_part2_naive() -> ce::Result<()> {
        let input = generate_input()?;
        assert_eq!(solve_part2_naive(&input), PART_2_ANSWER);
        Ok(())
    }

    #[test]
    fn it_solves_part2_involved() -> ce::Result<()> {
        let input = generate_input()?;
        assert_eq!(solve_part2_involved(&input), PART_2_ANSWER);
        Ok(())
    }

    fn generate_input() -> ce::Result<Vec<Option<u32>>> {
        let input = std::fs::read_to_string(INPUT)?;
        generate_calories(&input)
    }
}
