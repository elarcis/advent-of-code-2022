use std::{collections::VecDeque, convert::Infallible, str::FromStr};

use aoc_runner_derive::aoc;

// Puzzle: https://adventofcode.com/2022/day/5

/// Solves part 2 by one-by-one operations.
///
#[aoc(day5, part1)]
fn solve_part1(input: &str) -> String {
    solve(input, OpType::Single)
}

/// Solves part 2 by preserving the order in which crates are picked-up.
///
#[aoc(day5, part2)]
fn solve_part2(input: &str) -> String {
    solve(input, OpType::Batched)
}

/// Solves both part 1 and 2 by separately parsing the initial configuration, all operations to be
/// applied, applying those operations then building a `String` out of all top-items.
///
/// Nothing interesting here, the  implementation will be documented in the `Columns` struct.
///
fn solve(input: &str, op_type: OpType) -> String {
    let mut columns = Columns::from_str(input).expect("parsing columns should always be Ok");
    get_operations(input).for_each(|op| columns.apply_op(op, op_type));
    columns.top_items()
}

/// Gets an iterator of each operation to apply to the crates.
///
/// All operations are parsed then converted to a 0-based column indexing system for later
/// simplicity.
///
/// Lines that cannot be parsed into a proper operation are ignored silently.
///
fn get_operations(input: &'_ str) -> impl '_ + Iterator<Item = MoveOp> {
    input
        .lines()
        .skip_while(|line| !line.starts_with("move"))
        .take_while(|line| line.starts_with("move"))
        .filter_map(|line| {
            let mut words = line.split_whitespace();
            words.next(); // skip "move"
            let amount = words.next()?;
            words.next(); // skip "from"
            let from = words.next()?;
            words.next(); // skip "to"
            let to = words.next()?;

            // Column indexes are 0-based in code and 1-based in the input, shift them by 1.
            Some(MoveOp {
                amount: usize::from_str(amount).ok()?,
                from: usize::from_str(from).ok()? - 1,
                to: usize::from_str(to).ok()? - 1,
            })
        })
}

/// A crate that is stacked and moved. Matches an ASCII character’s byte.
type Crate = u8;

/// The state struct for day 5.
///
/// Each column is a `VecDeque<Item>` to facilitate initialising values and popping and pushing
/// items when solving the puzzle.
///
#[derive(Clone, Debug)]
struct Columns {
    columns: Vec<VecDeque<Crate>>,
}

impl Columns {
    /// Returns an empty `Columns`.
    ///
    fn new() -> Self {
        Self { columns: vec![] }
    }

    /// Gets a mutable borrow of one of the columns.
    ///
    /// If the column does not exist, `None` will be returned.
    ///
    fn get_mut(&mut self, column: usize) -> Option<&mut VecDeque<Crate>> {
        self.columns.get_mut(column)
    }

    /// Push an item to a column **by the bottom**. This is to be used when initialising the puzzle,
    /// all other modifications should be done via `apply_op()`.
    ///
    fn push_front(&mut self, column: usize, item: Crate) {
        while self.columns.len() < column + 1 {
            self.columns.push(VecDeque::new());
        }

        self.columns
            .get_mut(column)
            .expect("the accumulator `Vec` should have been preemptively extended")
            .push_front(item);
    }

    /// Applies an operation to the crates. The `op_type` parameter determines whether the order of
    /// items is reversed (part 1 of the puzzle) or preserved (part 2).
    ///
    fn apply_op(&mut self, op: MoveOp, op_type: OpType) {
        let mut items = VecDeque::new();

        for from in std::iter::repeat(op.from).take(op.amount) {
            if let Some(item) = self.get_mut(from).and_then(VecDeque::pop_back) {
                match op_type {
                    OpType::Batched => items.push_front(item),
                    OpType::Single => items.push_back(item),
                }
            }
        }

        if let Some(to) = self.get_mut(op.to) {
            to.extend(items);
        }
    }

    /// Returns a `String` built from items at the top of each non-empty column.
    ///
    fn top_items(&self) -> String {
        let bytes = self
            .columns
            .iter()
            .filter_map(VecDeque::back)
            .copied()
            .collect();
        String::from_utf8(bytes).expect("all items should be valid UTF-8 single-byte chars")
    }
}

impl FromStr for Columns {
    type Err = Infallible;

    /// Creates a `Columns` from an `input`.
    ///
    /// All invalid input is ignored. Therefore, this method never fails and can be unwrapped
    /// without concern.
    ///
    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let mut columns = Columns::new();

        // Stop adding items when we’re at the column numbers line
        for line in input.lines().take_while(|line| !line.starts_with(" 1")) {
            let mut column_index = 0;
            let mut chars = line.chars();

            while chars.next().is_some() {
                // ^^^ Skip "[" if existent
                if let Some(id) = chars.next() {
                    if !id.is_whitespace() {
                        let mut b = [0];
                        id.encode_utf8(&mut b);
                        columns.push_front(column_index, b[0]);
                    }

                    column_index += 1;

                    // Skip "] " if existent
                    chars.next();
                    chars.next();
                }
            }
        }

        Ok(columns)
    }
}

/// Type of operation to apply (matching respectively part 1 and part 2 of the puzzle).
///
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum OpType {
    /// All crates are moved one-by-one.
    Single,
    /// All crates are loaded up and dropped in a single move of the crane.
    Batched,
}

/// An operation to move crates from a column to another.
///
#[derive(Debug, Clone, Copy, PartialEq)]
struct MoveOp {
    amount: usize,
    from: usize,
    to: usize,
}

#[cfg(test)]
mod tests {
    use color_eyre as ce;
    use indoc::indoc;

    use super::*;

    const INPUT: &str = "./input/2022/day5.txt";
    const TEST_INPUT: &str = indoc! { r#"
            [D]
        [N] [C]
        [Z] [M] [P]
         1   2   3

        move 1 from 2 to 1
        move 3 from 1 to 3
        move 2 from 2 to 1
        move 1 from 1 to 2
    "#};

    /// Correct answer for the part 1 using the test input
    const PART_1_TEST_ANSWER: &'static str = "CMZ";
    /// Correct answer for the part 2 using the test input
    const PART_2_TEST_ANSWER: &'static str = "MCD";

    /// Correct answer for the part 1
    const PART_1_ANSWER: &'static str = "NTWZZWHFV";
    /// Correct answer for the part 2
    const PART_2_ANSWER: &'static str = "BRZGFVBTJ";

    #[test]
    fn it_solves_part1_test() {
        assert_eq!(solve_part1(TEST_INPUT), PART_1_TEST_ANSWER);
    }

    #[test]
    fn it_solves_part1() -> ce::Result<()> {
        let input = std::fs::read_to_string(INPUT)?;
        assert_eq!(solve_part1(&input), PART_1_ANSWER);
        Ok(())
    }

    #[test]
    fn it_solves_part2_test() {
        assert_eq!(solve_part2(TEST_INPUT), PART_2_TEST_ANSWER);
    }

    #[test]
    fn it_solves_part2() -> ce::Result<()> {
        let input = std::fs::read_to_string(INPUT)?;
        assert_eq!(solve_part2(&input), PART_2_ANSWER);
        Ok(())
    }

    #[test]
    fn it_gets_crates() {
        fn get_items_slice(cols: &mut Columns, column: usize) -> &mut [Crate] {
            cols.get_mut(column).unwrap().make_contiguous()
        }

        let mut cols =
            Columns::from_str(TEST_INPUT).expect("parsing to columns should always be Ok");
        assert_eq!(get_items_slice(&mut cols, 0), [b'Z', b'N']);
        assert_eq!(get_items_slice(&mut cols, 1), [b'M', b'C', b'D']);
        assert_eq!(get_items_slice(&mut cols, 2), [b'P']);
    }

    #[test]
    #[rustfmt::skip]
    fn it_gets_operations() {
        let mut ops = get_operations(TEST_INPUT);
        #[rustfmt::skip]
        assert_eq!(ops.next(), Some(MoveOp { amount: 1, from: 1, to: 0 }));
        #[rustfmt::skip]
        assert_eq!(ops.next(), Some(MoveOp { amount: 3, from: 0, to: 2 }));
        #[rustfmt::skip]
        assert_eq!( ops.next(), Some(MoveOp { amount: 2, from: 1, to: 0 }));
        #[rustfmt::skip]
        assert_eq!(ops.next(), Some(MoveOp { amount: 1, from: 0, to: 1 }));
    }
}
