use std::{ops::RangeInclusive, str::FromStr};

use aoc_runner_derive::aoc;

// Puzzle: https://adventofcode.com/2022/day/4

/// Solves part 1 by parsing each line as a pair of ranges, then "naively" comparing whether one of
/// the ranges contains the other.
///
#[must_use]
#[aoc(day4, part1)]
pub fn solve_part1(input: &str) -> usize {
    get_pairs::<u8>(input)
        .filter(|(first, second)| pairs_fully_overlap(first, second))
        .count()
}

/// Solves part 2 by parsing each line as a pair of ranges, then "naively" comparing whether one of
/// the ranges contains either end of the other.
///
#[must_use]
#[aoc(day4, part2)]
pub fn solve_part2(input: &str) -> usize {
    get_pairs::<u8>(input)
        .filter(|(first, second)| pairs_partially_overlap(first, second))
        .count()
}

/// Parses the input into an iterator of pairs of elves.
///
/// Each line is effectively parsed, if possible, into a tuple of ranges. Lines that could not be
/// parsed will be ignored silently.
///
fn get_pairs<T>(input: &'_ str) -> impl '_ + Iterator<Item = (RangeInclusive<T>, RangeInclusive<T>)>
where
    T: PartialOrd + FromStr,
{
    input.lines().filter_map(|line| {
        let (first, second) = line.split_once(',')?;
        let first = parse_range::<T>(first)?;
        let second = parse_range::<T>(second)?;
        Some((first, second))
    })
}

/// Parses a range from an input. Both ends of the range must be separated by `'-'`.
///
/// The order of the bounds isn’t checked, e.g. `"2-1"` will be correctly parsed into `2..=1`.
///
/// If `input` doesn’t have a `'-'` or if either bound of the range could not be parsed, `None` will
/// be returned.
///
fn parse_range<T>(input: &str) -> Option<RangeInclusive<T>>
where
    T: FromStr,
{
    let (start, end) = input.split_once('-')?;
    Some(start.parse().ok()?..=end.parse().ok()?)
}

/// Returns `true` if `first` contains `second` or vice-versa.
///
/// This approach works naively by testing one range against the other, then doing the same test
/// again by flipping the position of both ranges. There may be a better solution.
///
fn pairs_fully_overlap<T>(first: &RangeInclusive<T>, second: &RangeInclusive<T>) -> bool
where
    T: PartialOrd,
{
    /// Returns `true` if `a` contains `b`.
    ///
    /// The `.contains()` method of ranges isn’t used there, as it would do two more comparison than
    /// is necessary each time: for example when comparing the starts of the ranges, we just need to
    /// know whether `a.start()` is strictly less than `b.start()`, while using `.contains()` would
    /// also compare `b.start()` to `a.end()`, which is pointless.
    ///
    fn range_contains_range<T>(a: &RangeInclusive<T>, b: &RangeInclusive<T>) -> bool
    where
        T: PartialOrd,
    {
        (a.start() <= b.start()) && (a.end() >= b.end())
    }

    range_contains_range(first, second) || range_contains_range(second, first)
}

/// Returns `true` if `first` shares any amount of items with `second`.
///
fn pairs_partially_overlap<T>(first: &RangeInclusive<T>, second: &RangeInclusive<T>) -> bool
where
    T: PartialOrd,
{
    /// Returns `true` if `a` contains either the start or end of `b`.
    ///
    /// Please note that this function isn’t symmetric: in the case of `a = 2..=8` and `b = 4..=5`,
    /// `a` would be found to overlap `b` as it contains its start end end, but the opposite would
    /// not be true, as `b` contains neither the start nor the end of `a`.
    ///
    fn range_overlaps_range<T>(a: &RangeInclusive<T>, b: &RangeInclusive<T>) -> bool
    where
        T: PartialOrd,
    {
        a.contains(b.start()) || a.contains(b.end())
    }

    range_overlaps_range(first, second) || range_overlaps_range(second, first)
}

#[cfg(test)]
mod tests {
    use color_eyre as ce;
    use indoc::indoc;

    use super::*;

    const INPUT: &str = "./input/2022/day4.txt";
    const TEST_INPUT: &str = indoc! { r#"
        2-4,6-8
        2-3,4-5
        5-7,7-9
        2-8,3-7
        6-6,4-6
        2-6,4-8
    "#};

    /// Correct answer for the part 1 using the test input
    const PART_1_TEST_ANSWER: usize = 2;
    /// Correct answer for the part 2 using the test input
    const PART_2_TEST_ANSWER: usize = 4;

    /// Correct answer for the part 1
    const PART_1_ANSWER: usize = 532;
    /// Correct answer for the part 2
    const PART_2_ANSWER: usize = 854;

    #[test]
    fn it_solves_part1_test() {
        assert_eq!(solve_part1(TEST_INPUT), PART_1_TEST_ANSWER);
    }

    #[test]
    fn it_solves_part1() -> ce::Result<()> {
        let input = std::fs::read_to_string(INPUT)?;
        assert_eq!(solve_part1(&input), PART_1_ANSWER);
        Ok(())
    }

    #[test]
    fn it_solves_part2_test() {
        assert_eq!(solve_part2(TEST_INPUT), PART_2_TEST_ANSWER);
    }

    #[test]
    fn it_solves_part2() -> ce::Result<()> {
        let input = std::fs::read_to_string(INPUT)?;
        assert_eq!(solve_part2(&input), PART_2_ANSWER);
        Ok(())
    }

    #[test]
    fn it_parses_ranges() {
        assert_eq!(parse_range("4-8"), Some(4..=8));
        assert_eq!(parse_range("4-4"), Some(4..=4));
        assert_eq!(parse_range("8-4"), Some(8..=4));
        assert_eq!(parse_range::<u8>("a-8"), None);
        assert_eq!(parse_range::<u8>("4 8"), None);
    }
}
