use aoc_runner_derive::{aoc, aoc_generator};

// Puzzle: https://adventofcode.com/2022/day/2

/// Parses every line of the input as a round of Rock Paper Scissor, assuming `X`, `Y` and `Z` are
/// moves. This function ignores any line that doesn’t match the expected pattern, as it would be
/// an invalid input anyway.
///
#[must_use]
#[aoc_generator(day2, part1)]
pub fn generate_plays(input: &str) -> Vec<(Move, Move)> {
    input
        .lines()
        .filter_map(|line| {
            let (them, you) = line.split_once(' ')?;
            let them = get_move(them.chars().next()?)?;
            let you = get_move_wrong(you.chars().next()?)?;
            Some((them, you))
        })
        .collect()
}

/// Parses every line of the input as a round of Rock Paper Scissor, but this time the second part
/// of the line is the desired outcome instead of a move. This function ignores any line that
/// doesn’t match the expected pattern, as it would be an invalid input anyway.
///
#[must_use]
#[aoc_generator(day2, part2)]
pub fn generate_outcomes(input: &str) -> Vec<(Move, Outcome)> {
    input
        .lines()
        .filter_map(|line| {
            let (them, outcome) = line.split_once(' ')?;
            let them = get_move(them.chars().next()?)?;
            let outcome = get_outcome(outcome.chars().next()?)?;
            Some((them, outcome))
        })
        .collect()
}

#[must_use]
#[aoc(day2, part1)]
pub fn solve_part1(input: &[(Move, Move)]) -> u32 {
    input
        .iter()
        .map(|(them, you)| get_score(*them, *you))
        .sum::<u32>()
}

#[must_use]
#[aoc(day2, part2)]
pub fn solve_part2(input: &[(Move, Outcome)]) -> u32 {
    input
        .iter()
        .map(|(them, outcome)| {
            let you = them.get_move_for(*outcome);
            get_score(*them, you)
        })
        .sum::<u32>()
}

/// A possible move in a game of Rock Paper Scissor.
///
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Move {
    Rock,
    Paper,
    Scissor,
}

impl Move {
    /// Gets the move that should be played to obtain the desired outcome against `self`.
    ///
    #[must_use]
    fn get_move_for(self, outcome: Outcome) -> Move {
        match (self, outcome) {
            (Move::Rock, Outcome::Draw)
            | (Move::Paper, Outcome::Lose)
            | (Move::Scissor, Outcome::Win) => Move::Rock,
            (Move::Rock, Outcome::Win)
            | (Move::Paper, Outcome::Draw)
            | (Move::Scissor, Outcome::Lose) => Move::Paper,
            (Move::Rock, Outcome::Lose)
            | (Move::Paper, Outcome::Win)
            | (Move::Scissor, Outcome::Draw) => Move::Scissor,
        }
    }

    /// Gets the outcome of a move played against `self`.
    ///
    #[must_use]
    fn get_outcome(self, other: Move) -> Outcome {
        match (self, other) {
            (Move::Rock, Move::Scissor)
            | (Move::Paper, Move::Rock)
            | (Move::Scissor, Move::Paper) => Outcome::Win,
            (Move::Rock, Move::Paper)
            | (Move::Paper, Move::Scissor)
            | (Move::Scissor, Move::Rock) => Outcome::Lose,
            _ => Outcome::Draw,
        }
    }
}

impl From<Move> for u32 {
    /// Converts the given `Move` into its score value.
    ///
    fn from(value: Move) -> Self {
        match value {
            Move::Rock => 1,
            Move::Paper => 2,
            Move::Scissor => 3,
        }
    }
}

/// The outcome of a round of Rock Paper Scissor.
///
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Outcome {
    Win,
    Draw,
    Lose,
}

impl From<Outcome> for u32 {
    /// Converts the given `Outcome` into its score value.
    fn from(value: Outcome) -> Self {
        match value {
            Outcome::Win => 6,
            Outcome::Draw => 3,
            Outcome::Lose => 0,
        }
    }
}

/// Gets a `Move` from a given `char` in the strategy guide.
/// In case the `char` is not `'A'`, `'B'` nor `'C'`, `None` is returned.
///
fn get_move(code: char) -> Option<Move> {
    match code {
        'A' => Some(Move::Rock),
        'B' => Some(Move::Paper),
        'C' => Some(Move::Scissor),
        _ => None,
    }
}

/// Gets a `Move` from a given `char` in the strategy guide based on the assumption made in part 1.
/// In case the `char` is not `'X'`, `'Y'` nor `'Z'`, `None` is returned.
///
fn get_move_wrong(code: char) -> Option<Move> {
    match code {
        'X' => Some(Move::Rock),
        'Y' => Some(Move::Paper),
        'Z' => Some(Move::Scissor),
        _ => None,
    }
}

/// Gets an `Outcome` from a given `char` in athe strategy guide based on the clarification brought
/// by part 2. In case the char is not `'X'`, `'Y'` nor `'Z'`, `None` is returned.
///
fn get_outcome(code: char) -> Option<Outcome> {
    match code {
        'X' => Some(Outcome::Lose),
        'Y' => Some(Outcome::Draw),
        'Z' => Some(Outcome::Win),
        _ => None,
    }
}

/// Gets the score obtained by the given exchange of moves.
///
/// The score is the one `you` gets, `other`’s score is discarded.
///
fn get_score(them: Move, you: Move) -> u32 {
    let your_score = u32::from(you);
    let outcome = you.get_outcome(them);
    let outcome_score = u32::from(outcome);
    outcome_score + your_score
}

#[cfg(test)]
mod tests {
    use color_eyre as ce;
    use indoc::indoc;

    use super::*;

    const INPUT: &str = "./input/2022/day2.txt";
    const TEST_INPUT: &str = indoc! {r#"
        A Y
        B X
        C Z
    "#};

    /// Correct answer for the part 1
    const PART_1_ANSWER: u32 = 13005;
    /// Correct answer for the part 2
    const PART_2_ANSWER: u32 = 11373;

    /// Checks the part 1 algorithm against the correct solution
    #[test]
    fn it_solves_part1() -> ce::Result<()> {
        let input = generate_input_part1()?;
        assert_eq!(solve_part1(&input), PART_1_ANSWER);
        Ok(())
    }

    /// Checks the part 2 algorithm against the correct solution
    #[test]
    fn it_solves_part2() -> ce::Result<()> {
        let input = generate_input_part2()?;
        assert_eq!(solve_part2(&input), PART_2_ANSWER);
        Ok(())
    }

    #[test]
    fn it_parses_plays() {
        let plays = generate_plays(TEST_INPUT);
        assert_eq!(
            plays,
            [
                (Move::Rock, Move::Paper),
                (Move::Paper, Move::Rock),
                (Move::Scissor, Move::Scissor)
            ]
        );
    }

    #[test]
    fn it_parses_outcomes() {
        let outcomes = generate_outcomes(TEST_INPUT);
        assert_eq!(
            outcomes,
            [
                (Move::Rock, Outcome::Draw),
                (Move::Paper, Outcome::Lose),
                (Move::Scissor, Outcome::Win)
            ]
        );
    }

    #[test]
    fn it_gets_rock_outcomes() {
        assert_eq!(Move::Rock.get_outcome(Move::Rock), Outcome::Draw);
        assert_eq!(Move::Rock.get_outcome(Move::Paper), Outcome::Lose);
        assert_eq!(Move::Rock.get_outcome(Move::Scissor), Outcome::Win);
    }

    #[test]
    fn it_gets_paper_outcomes() {
        assert_eq!(Move::Paper.get_outcome(Move::Rock), Outcome::Win);
        assert_eq!(Move::Paper.get_outcome(Move::Paper), Outcome::Draw);
        assert_eq!(Move::Paper.get_outcome(Move::Scissor), Outcome::Lose);
    }

    #[test]
    fn it_gets_scissor_outcomes() {
        assert_eq!(Move::Scissor.get_outcome(Move::Rock), Outcome::Lose);
        assert_eq!(Move::Scissor.get_outcome(Move::Paper), Outcome::Win);
        assert_eq!(Move::Scissor.get_outcome(Move::Scissor), Outcome::Draw);
    }

    fn generate_input_part1() -> ce::Result<Vec<(Move, Move)>> {
        let input = std::fs::read_to_string(INPUT)?;
        Ok(generate_plays(&input))
    }

    fn generate_input_part2() -> ce::Result<Vec<(Move, Outcome)>> {
        let input = std::fs::read_to_string(INPUT)?;
        Ok(generate_outcomes(&input))
    }
}
